let fruits = ["🍏", "🍓","🍌","🍍"];

function init(){//this is to initialise the game, to add my fruits to the interface
    let out = document.getElementById('fruitsout');
    let str = fruits.join("<br>")
    out.innerHTML = str;
}

function unShiftFruit(){
    let fruitinput = document.getElementById('fruit')
    if(fruitinput.value=="") return false
    fruits.unshift(fruitinput.value)
    init()
}

function pushFruit(){
    let fruitinput = document.getElementById('fruit')
    if(fruitinput.value=="") return false
    fruits.push(fruitinput.value)
    init()
}


function popfruit(){
    let fruitinput = document.getElementById("fruit")
    fruits.pop()
    init()
}

function shiftfruit(){
    let fruitinput = document.getElementById("fruit")
    fruits.shift()
    init()
}

