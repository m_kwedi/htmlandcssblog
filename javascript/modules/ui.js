import  Employee  from "./employee.js";

export class UI extends Employee {
    constructor(name, arrivalTime, departureTime, output, employeesList) {
        super(name, arrivalTime, departureTime) 
        this.output = output;
        this.employeesList = employeesList;
        this.paintOutput()
    }


    initialiseInputs(nameInput, arrivalInput, departureInput) {
        this.nameInput = nameInput
        this.departureInput = departureInput
        this.arrivalInput = arrivalInput
    }

    setName() {
        this.name = this.nameInput.value;
    }

    setArrivalTime() {
        this.arrivalTime = this.arrivalInput.value;
    }

    setDepartureTime() {
        this.departureTime = this.departureInput.value;
    }

    saveTolist() {
        try {
            this.setName()
            this.setArrivalTime()
            this.setDepartureTime()
            let employee = this.addD()
            this.employeesList.push(employee) 
            localStorage.setItem('employees',JSON.stringify(this.employeesList))
            this.cleanInputs()
            this.paintOutput()
        } catch (e) {
            alert(e.message)
        }
    }

    setEmployeesList(){        
        this.employeesList = JSON.parse(localStorage.getItem('employees')||'[]');
    }

    paintOutput() { 
        this.output.innerHTML = this.determineOuput()
    }

    cleanInputs() {
        this.nameInput.value = "";
        this.departureInput.value = "";
        this.arrivalInput.value = "";
    }
    /*
    this app require q list or a tablebody to output values
    */
    determineOuput() {
        this.setEmployeesList()
        switch (this.output.tagName.toUpperCase()) {
            case 'TBODY':
                console.log(this.employeesList);
                return this.employeesList.map((emp) =>
                    `<tr><td>${emp.name}</td><td>${emp.arrivalTime}</td><td>${emp.departureTime}</td></tr>`
                ).join('')
                break;
            case 'UL':
            case 'OL':
                return this.employeesList.map((emp) =>
                    `<li><h5>${emp.name}</h5><strong>Arrival:</strong> ${emp.arrivalTime}<br><strong>Departure:</strong> ${emp.departureTime}</li>`
                ).join('')
                break;
        }
    }

} 