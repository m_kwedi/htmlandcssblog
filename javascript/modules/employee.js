export default class Employee {
    constructor(name,arrivalTime,departureTime){
        this.name = name;
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
    }

    addD(){
        if(!this.name || !this.arrivalTime || !this.departureTime){
            throw Error("Need to set Name and hours")
        }
        let that = this;
        return {
            name:that.name,
            arrivalTime:that.arrivalTime,
            departureTime:that.departureTime
        }
    }
}
 